__author__ = 'Janno Siim'
from LDPC import *
from copy import deepcopy

passed = True
failed_tests = []

#Test_swap1
rows = [[0],[0,1],[2]]
cols = [[0,1], [1], [2]]
swap_rows(0,2, rows, cols)
out_rows = [[2], [0,1], [0]]
out_cols = [[1,2], [1], [0]]

if rows != out_rows or cols != out_cols:
    passed = False
    failed_tests.append("Test_swap1")

#Test_swap2
rows2 = [[0,3],[2,4],[0,1,2],[4]]
cols2 = [[0,2],[2],[1,2],[0],[1,3]]
swap_rows(1,3, rows2, cols2)
out_rows2 = [[0,3],[4],[0,1,2],[2,4]]
out_cols2 = [[0,2],[2], [2,3], [0], [1,3]]

if rows2 != out_rows2 or cols2 != out_cols2:
    passed = False
    failed_tests.append("Test_swap2")

#Test_swap3
rows3 = [[0],[0,1],[1],[],[0]]
cols3 = [[0,1,4], [1,2]]
swap_rows(1,3, rows3, cols3)
out_rows3 = [[0],[],[1],[0,1],[0]]
out_cols3 = [[0,3,4], [2,3]]


if rows3 != out_rows3 or cols3 != out_cols3:
    passed = False
    failed_tests.append("Test_swap3")

#Test_swap4
rows4 = [[1, 2, 3], [0]]
cols4 = [[1], [0], [0], [0]]
swap_rows(0, 1, rows4, cols4)
out_rows4 = [[0], [1, 2, 3]]
out_cols4 = [[0], [1], [1], [1]]

if rows4 != out_rows4 or cols4 != out_cols4:
    passed = False
    failed_tests.append("Test_swap4")

#Test4
rows4 = [[2,4,10],
         [0,3,4,7]]
cols4 = [[1],[],[0],[1],[0,1],[],[],[1],[],[],[0]]
add_rows(1,0, rows4, cols4)
out_rows4 = [[2,4,10], [0,2,3,7,10]]
out_cols4 = [[1],[],[0,1],[1],[0],[],[],[1],[],[],[0,1]]
if rows4 != out_rows4 or cols4 != out_cols4:
    passed = False
    failed_tests.append("Test4")

#Test5
rows5 = [[2,4,10],
         [0,3,4,7]]
cols5 = [[1],[],[0],[1],[0,1],[],[],[1],[],[],[0]]
add_rows(0,1, rows5, cols5)
out_rows5 = [[0,2,3,7,10], [0,3,4,7]]
out_cols5 = [[0,1],[],[0],[0,1],[1],[],[],[0,1],[],[],[0]]

if rows5 != out_rows5 or cols5 != out_cols5:
    passed = False
    failed_tests.append("Test5")

#Test6
rows6 = [[],
         [0,3,4,7]]
cols6 = [[1],[],[],[1],[1],[],[],[1],[],[],[]]
add_rows(0,1, rows6, cols6)
out_rows6 = [[0,3,4,7], [0,3,4,7]]
out_cols6 = [[0,1],[],[],[0,1],[0,1],[],[],[0,1],[],[],[]]

if rows6 != out_rows6 or cols6 != out_cols6:
    passed = False
    failed_tests.append("Test6")

#Test7
rows7 = [[],
         [0,3,4,7]]
cols7 = [[1],[],[],[1],[1],[],[],[1],[],[],[]]
add_rows(1,0, rows7, cols7)
out_rows7 = [[], [0,3,4,7]]
out_cols7 = [[1],[],[],[1],[1],[],[],[1],[],[],[]]

if rows7 != out_rows7 or cols7 != out_cols7:
    passed = False
    failed_tests.append("Test7")

#Test8
rows8 = [[],
         []]
cols8 = [[], [], [], [], [], [], [], [], [], [], []]
add_rows(1, 0, rows8, cols8)
out_rows8 = [[], []]
out_cols8 = [[], [], [], [], [], [], [], [], [], [], []]

if rows8 != out_rows8 or cols8 != out_cols8:
    passed = False
    failed_tests.append("Test8")


#Test9
rows9 = [[1, 2], [0, 1], [], [2], []]
cols9 = [[1], [0, 1], [0, 3]]
add_rows(3, 0, rows9, cols9)
out_rows9 = [[1, 2], [0, 1], [], [1], []]
out_cols9 = [[1], [0, 1, 3], [0]]

if rows9 != out_rows9 or cols9 != out_cols9:
    passed = False
    failed_tests.append("Test9")

#Test10 (m == n)
rows10 = [[0, 2], [1], [0, 1]]
cols10 = [[0, 2], [1, 2], [0]]
constants10 = [1, 0, 0]
solution10 = [0, 0, 1]

if solve_linear_system(rows10, cols10, constants10) != solution10:
    passed = False
    failed_tests.append("Test 10")


#Test11 (m < n)
rows11 = [[0, 1], [2, 3]]
cols11 = [[0], [0], [1], [1]]
constants11 = [1, 0]
solution11 = None

if solve_linear_system(rows11, cols11, constants11) != solution11:
    passed = False
    failed_tests.append("Test 11")

#Test12 (m > n)
rows12 = [[1, 2], [2], [0, 2], [0, 1]]
cols12 = [[2, 3], [0, 3], [0, 1, 2]]
constants12 = [1, 0, 0, 1]
solution12 = [0, 1, 0]

if solve_linear_system(rows12, cols12, constants12) != solution12:
    passed = False
    failed_tests.append("Test 12")


#Test13 (inconsistent)
rows13 = [[1, 2], [2], [], [0, 2], [0]]
cols13 = [[3, 4], [0], [0, 1, 3]]
constants13 = [1, 1, 0, 1, 1]
solution13 = None


if solve_linear_system(rows13, cols13, constants13) != solution13:
    passed = False
    failed_tests.append("Test 13")

#Test14 (big system)

rows14 = [[0, 2, 3], [1, 2, 4], [0, 1, 2, 3, 4], [], [2, 4], [0, 1, 3], [1, 3]]
cols14 = [[0, 2, 5], [1, 2, 5, 6], [0, 1, 2, 4], [0, 2, 5, 6], [1, 2, 4]]
constants14 = [0, 1, 1, 0, 1, 0, 1]
solution14 = [1, 0, 0, 1, 1]

if solve_linear_system(rows14, cols14, constants14) != solution14:
    passed = False
    failed_tests.append("Test 14")

#Test15 (No unique solution)
rows15 = [[0, 1], [2, 3], [0, 1], [2, 3]]
cols15 = [[0, 2], [0, 2], [1, 3], [1, 3]]
constants15 = [1, 1, 1, 1]
solution15 = None

if solve_linear_system(rows15, cols15, constants15) != solution15:
    passed = False
    failed_tests.append("Test 15")

#Test16 (Big solvable)

rows16 = [[1, 2, 3], [0], [0, 1, 2, 3], [0, 3], [0, 1]]
cols16 = [[1, 2, 3, 4], [0, 2, 4], [0, 2], [0, 2, 3]]
constants16 = [1, 0, 1, 1, 1]
solution16 = [0, 1, 1, 1]

if solve_linear_system(rows16, cols16, constants16) != solution16:
    passed = False
    failed_tests.append("Test 16")

#G_Dec test1
rows_g1 = [[0, 1, 2], [0, 2], [1, 2]]
cols_g1 = [[0, 1], [0, 2], [0, 1, 2]]
partial_codeword_g1 = [0, '?', 0]
code_word_g1 = [0, 0, 0]

if Gaussian_decoding((rows_g1, cols_g1), partial_codeword_g1) != code_word_g1:
    passed = False
    failed_tests.append("Test G_dec_1")

while passed:
    #2-elimination test1,2
    parity_mat = generate_parity_matrix(6, 3, 2)
    parity_mat_copy = deepcopy(parity_mat)
    code_word = find_code_word(parity_mat)
    received_word = BEC_simulator(code_word, 0.3)
    print received_word
    received_word_copy = received_word[:]

    response = two_elimination_decoding( parity_mat, received_word)

    if response:
        if code_word != received_word:
            passed = False
            print "        Code word:" + str(code_word)
            print "Incorrectly fixed:" + str(received_word)
            failed_tests.append("2-elimination test1")
    else:
        if '?' not in received_word:
            passed = False
            failed_tests.append("2-elimination test2")

        for i in range(len(received_word)):
            if received_word[i] != '?' and code_word[i] != received_word[i]:
                passed = False
                print "----"
                print "code word: " + str(code_word)
                print "received word: " + str(received_word_copy)
                print "wrong word: " + str(received_word)
                print "parit mat:" + str(parity_mat_copy)
                failed_tests.append("2-elimination test3")
                break

# if parity_mat != parity_mat_copy:
#     passed = False
#     failed_tests.append("2-elimination test4")

#Two elimination test 5
rows = [[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]]
cols = [[0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]]
parity_mat = (rows, cols)

code_word = [1, 0, 0, 1, 1, 1]
received_word = [1, 0, 0, 1, '?', '?']
correct_word = [1, 0, 0, 1, '?', '?']

two_elimination_decoding( parity_mat, received_word)
if received_word != correct_word:
    passed = False
    failed_tests.append("2-elimination test5")

#Two elimination test 6
rows = [[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]]
cols = [[0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]]
parity_mat = (rows, cols)

code_word = [0, 1, 1, 1, 1, 0]
received_word = [0, 1, '?', '?', 1, 0]
fixed = two_elimination_decoding( parity_mat, received_word)

if fixed:
    passed = False
    failed_tests.append("2-elimination test6")

#Two elimination test 7
rows = [[0,1,2,3,4,5], [6,7,8,9,10,11], [0, 2, 4, 6, 7,10], [1,3,5,8,9,11], [0,1,2,4,5,11], [3,6,7,8,9,10]]
cols = [[0,2,4], [0,3,4], [0,2,4], [0, 3, 5], [0,2,4], [0,3,4], [1,2,5], [1,2,5], [1,3,5], [1,3,5], [1,2,5], [1,3,4]]
parity_mat = (rows, cols)
correct_word = [1,1,1,1,1,1,0,1,0,0,0,1]
received_word = [1, '?', 1, 1, '?', '?', 0, '?', '?', 0, 0, '?']
two_elimination_decoding( parity_mat, received_word)

for i in range(len(received_word)):
    if received_word[i] != '?' and received_word[i] != correct_word[i]:
        passed = False
        failed_tests.append("2-elimination test7")

#Two elimination test 8

rows = [[0, 1, 2, 3, 4, 5], [6, 7, 8, 9, 10, 11], [2, 3, 5, 6, 10, 11], [0, 1, 4, 7, 8, 9], [1, 2, 4, 8, 9, 11], [0, 3, 5, 6, 7, 10]]
cols = [[0, 3, 5], [0, 3, 4], [0, 2, 4], [0, 2, 5], [0, 3, 4], [0, 2, 5], [1, 2, 5], [1, 3, 5], [1, 3, 4], [1, 3, 4], [1, 2, 5], [1, 2, 4]]
parity_mat = (rows, cols)
correct_word = [1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1]
received_word = ['?', 0, '?', 1, 0, 0, 1, '?', '?', 0, 1, '?']
two_elimination_decoding( parity_mat, received_word)

for i in range(len(received_word)):
    if received_word[i] != '?' and received_word[i] != correct_word[i]:
        passed = False
        failed_tests.append("2-elimination test8")

#Two elimination test 9
rows = [[0, 1, 2, 3, 4, 5], [6, 7, 8, 9, 10, 11], [1, 2, 3, 4, 8, 9], [0, 5, 6, 7, 10, 11], [1, 2, 3, 5, 6, 11], [0, 4, 7, 8, 9, 10]]
cols = [[0, 3, 5], [0, 2, 4], [0, 2, 4], [0, 2, 4], [0, 2, 5], [0, 3, 4], [1, 3, 4], [1, 3, 5], [1, 2, 5], [1, 2, 5], [1, 3, 5], [1, 3, 4]]
parity_mat = (rows, cols)
correct_word = [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0]
received_word = ['?', 0, '?', 0, 0, 1, '?', '?', '?', '?', 1, 0]
two_elimination_decoding( parity_mat, received_word)

for i in range(len(received_word)):
    if received_word[i] != '?' and received_word[i] != correct_word[i]:
        passed = False
        failed_tests.append("2-elimination test9")


#Two elimination test 10
rows = [[0, 1, 2, 3, 4, 5], [6, 7, 8, 9, 10, 11], [1, 4, 5, 6, 7, 9], [0, 2, 3, 8, 10, 11], [1, 3, 5, 6, 8, 9], [0, 2, 4, 7, 10, 11]]
cols = [[0, 3, 5], [0, 2, 4], [0, 3, 5], [0, 3, 4], [0, 2, 5], [0, 2, 4], [1, 2, 4], [1, 2, 5], [1, 3, 4], [1, 2, 4], [1, 3, 5], [1, 3, 5]]
parity_mat = (rows, cols)
correct_word = [1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0]
received_word = ['?', 1, 0, '?', 0, '?', 1, 0, 1, '?', 1, '?']
two_elimination_decoding( parity_mat, received_word)

for i in range(len(received_word)):
    if received_word[i] != '?' and received_word[i] != correct_word[i]:
        passed = False
        failed_tests.append("2-elimination test10")

if passed:
    print "Tests passed"
else:
    print "Failed test" + str(failed_tests)
__author__ = 'janno'
from LDPC import *
import time
from copy import deepcopy

'''
Returns number of errors in code word.
'''
def error_count(word):
    return len([bit for bit in word if bit == '?'])


def bit_error_rate_experiment(n, iterations, increment1, increment2, path):

    C = 3
    D = 6


    error_sum = {}

    for i in range(iterations):
        p = 0.01
        print "iteration: " + str(i)

        while p <= 0.4:

            parity_mat = generate_parity_matrix(D, C, n)
            #parity_mat_copy = deepcopy(parity_mat)

            code_word = get_zero_codeword(parity_mat)
            received_word = BEC_simulator(code_word, p)

            iterative_decoding(parity_mat, received_word)
            #two_elimination_decoding(parity_mat, received_word)
            #Gaussian_decoding(parity_mat_copy, received_word)

            error_sum[p] = error_sum.get(p, 0.0) + float(error_count(received_word))/float(D*n)

            p += increment1


        p = p - increment1 + increment2
        while p <= 0.5:

            parity_mat = generate_parity_matrix(D, C, n)
            #parity_mat_copy = deepcopy(parity_mat)

            code_word = get_zero_codeword(parity_mat)
            received_word = BEC_simulator(code_word, p)

            iterative_decoding(parity_mat, received_word)
            #two_elimination_decoding(parity_mat, received_word)
            #Gaussian_decoding(parity_mat_copy, received_word)

            error_sum[p] = error_sum.get(p, 0.0) + float(error_count(received_word))/float(D*n)

            p += increment2


        p = p - increment2 + increment1
        while p <= 0.99:
            parity_mat = generate_parity_matrix(D, C, n)
            #parity_mat_copy = deepcopy(parity_mat)

            code_word = get_zero_codeword(parity_mat)
            received_word = BEC_simulator(code_word, p)

            iterative_decoding(parity_mat, received_word)
            #two_elimination_decoding(parity_mat, received_word)
            #Gaussian_decoding(parity_mat_copy, received_word)

            error_sum[p] = error_sum.get(p, 0.0) + float(error_count(received_word))/float(D*n)
            p += increment1

    f = open(path, "w")

    for p in sorted(error_sum.keys()):
        f.write(str(p) + ", " + str(error_sum[p]/float(iterations)) + "\n")

    f.close()

def time_measurement_experiment(p, max_n, iterations, path):

    time_sum_dict = {}
    for i in range(iterations):
        print i
        for n in range(1, max_n, 50):
            parity_mat = generate_parity_matrix(6, 3, n)
            parity_mat_copy = deepcopy(parity_mat)
            code_word = get_zero_codeword(parity_mat)
            received_word = BEC_simulator(code_word, p)

            start = time.time()
            iterative_decoding(parity_mat, received_word)
            #two_elimination_decoding(parity_mat, received_word)
            Gaussian_decoding(parity_mat_copy, received_word)
            time_dif = time.time() - start

            time_sum_dict[n] = time_sum_dict.get(n, 0) + time_dif

    f = open(path, "w")
    for n in sorted(time_sum_dict.keys()):
        f.write(str(n) + ", " + str(float(time_sum_dict[n])/float(iterations)) + "\n")
    f.close()


# iterations = 50
# n = 300
# increment1 = 0.05
# increment2 = 0.01
#
# path = "../measurements/bit_error_rate_N" + str(iterations) + "_n" + str(n) + "ID.txt"
# bit_error_rate_experiment(n, iterations, increment1, increment2, path)


# p = 0.44
# max_n = 500
# iterations = 20
# path = "../measurements/time_n" + str(max_n)  + "N" + str(iterations) + "p" + str(p) + "ID+G.txt"
# time_measurement_experiment(p, max_n, iterations, path)


k = 10    #size
p = 0.4   #erasure probability

found = False
while not found:
    used_2el = False
    parity_mat = generate_parity_matrix(6, 3, k)

    print "parity_mat: " + str(parity_mat)
    code_word = find_code_word(parity_mat)
    #code_word = get_zero_codeword(parity_mat)
    # #
    print "       code word: " + str(code_word)
    # #
    received_word = BEC_simulator(code_word, p)
    received_word_copy = received_word[:]
    print "   received word: " + str(received_word)
    #
    #iterative_decoding(parity_mat, received_word)
    # print "decoded word(ID): " + str(received_word)
    #
    used_2el = two_elimination_decoding( parity_mat, received_word_copy)[0]
    print "decoded word(2E): " + str(received_word_copy)

    if '?' not in received_word and used_2el:
        found = True
    # #
    # print "decoded word(GD): " + str(Gaussian_decoding(parity_mat, received_word))


__author__ = 'Janno Siim'

from random import shuffle, uniform, randint
from binary_mat import *



'''
Generates sparse parity-check matrix of size nr_of_c_ones*k x nr_of_r_ones*k over field F_2.
Every row has nr_of_r_ones of ones and every column has nr_of_c_ones of ones.
Returns pair (rows, cols) where rows[i][j] = k if in the matrix i-th row contains
one in column k and similarly cols[i][j] = k if column i contains one in the k-th row.
rows[i] and cols[i] are sorted.
'''
def generate_parity_matrix(nr_of_r_ones, nr_of_c_ones, k):
    rows = []
    cols = []

    row = []
    for i in range(nr_of_r_ones * k):

        row.append(i)
        cols.append([i // nr_of_r_ones])

        if i % nr_of_r_ones == nr_of_r_ones - 1:
            rows.append(row)
            row = []

    for i in range(nr_of_c_ones -1):
        indices = range(0, nr_of_r_ones*k)
        shuffle(indices)

        row = []
        for j in range(len(indices)):
            row.append(indices[j])
            cols[indices[j]].append(k*(i+1) + (j // nr_of_r_ones))

            if j % nr_of_r_ones == nr_of_r_ones - 1:
                rows.append(sorted(row))
                row = []

    #sort lists in cols
    for i in range(len(cols)):
        cols[i] = sorted(cols[i])

    return (rows, cols)


def generate_parity_matrix(nr_of_r_ones, nr_of_c_ones, k):
    rows = []
    cols = []

    row = []
    for i in range(nr_of_r_ones * k):

        row.append(i)
        cols.append([i // nr_of_r_ones])

        if i % nr_of_r_ones == nr_of_r_ones - 1:
            rows.append(row)
            row = []

    for i in range(nr_of_c_ones -1):
        indices = range(0, nr_of_r_ones*k)
        shuffle(indices)

        row = []
        for j in range(len(indices)):
            row.append(indices[j])
            cols[indices[j]].append(k*(i+1) + (j // nr_of_r_ones))

            if j % nr_of_r_ones == nr_of_r_ones - 1:
                rows.append(sorted(row))
                row = []

    #sort lists in cols
    for i in range(len(cols)):
        cols[i] = sorted(cols[i])

    return (rows, cols)

'''
Returns codeword of all zeros.
'''
def get_zero_codeword(parity_mat):
 return [0]*len(parity_mat[1])


'''
Find a random codeword. Currently inefficient and should be improved.
'''
def find_code_word(parity_mat):

    random_codeword = None
    while random_codeword == None:
        random_codeword = []
        for i in range(len(parity_mat[1])):
            number = uniform(0,1)
            if number < 0.50:
                random_codeword.append(randint(0,1))
            else:
                random_codeword.append('?')

        random_codeword = Gaussian_decoding(parity_mat, random_codeword)

    return random_codeword

'''
Takes in parity-check matrix and a bit vector.
Returns true if bit vector is a code word.
'''
def is_code_word(parity_mat, code_word):
    for row in parity_mat[0]:
        sum = 0
        for i in row:
            sum = sum ^ code_word[i]

        if sum != 0:
            return False

    return True


'''
Simulates binary erasure channel(BEC) with erasure probability p.
Every bit in the code word is replaced with '?' with probability p.
p is float, code_word is array of zeros and ones.
'''
def BEC_simulator(code_word, p):
    received_word = []
    for bit in code_word:
        nr = uniform(0,1)
        if nr <= p:
            received_word.append('?')
        else:
            received_word.append(bit)

    return received_word


'''
 Runs iterative decoding algorithm on the received_word. Fixes as many erasures as
 possible. Returns true if codeword was fully fixed and otherwise false.
 '''
def iterative_decoding(parity_mat, received_word, removed_rows = set(), removed_cols = set(), check_node_value = []):
    rows = parity_mat[0]

    row_indices = set()
    for i in range(len(rows)):
        removed_rows
        if i not in removed_rows:
            row_indices.add(i)

    code_word_fixed = False
    stuck = False
    while not stuck:
        code_word_fixed = True
        stuck = True

        to_be_removed = set()#after every iteration some row indices are removed
        for i in row_indices:
            missingBitCount = len([1 for j in rows[i] if j not in removed_cols and received_word[j] == '?'])

            if missingBitCount != 0:
                code_word_fixed = False
            else:
                to_be_removed.add(i)

            if missingBitCount == 1:
                position = next(k for k in rows[i] if received_word[k] == '?' and k not in removed_cols)
                constant_list = [received_word[j] for j in rows[i] if received_word[j] != '?']
                received_word[position] = 0

                if len(constant_list) > 0:
                    received_word[position] = reduce(lambda x, y: x^y, constant_list)
                if len(check_node_value) != 0:
                    received_word[position] ^= check_node_value[i]

                stuck = False
                to_be_removed.add(i)
        row_indices.difference_update(to_be_removed)

    return code_word_fixed

'''
 Runs two elimination algorithm together with iterative decoding on the received_word. Fixes as many erasures as
 possible. Returns true if codeword was fully fixed and otherwise false.
 '''

def two_elimination_decoding(parity_mat, received_word):
    used_2el = False
    rows = parity_mat[0]
    cols = parity_mat[1]
    fixed = False

    removed_row_set = set()
    removed_column_set = set()

    #substitution_dict[i] = (j, c). Meaning  y_i = y_j + c
    substitution_dict = {}
    check_node_value = [0]*len(rows)

    while not fixed:
        #Fix as much as possible
        fixed = iterative_decoding(parity_mat, received_word, removed_row_set, removed_column_set, check_node_value)

        if fixed:
            #Substitute back the removed variables
            for i in substitution_dict:
                index, constant = substitution_dict[i]
                if received_word[index] != '?':
                    received_word[i] = received_word[index] ^ constant
                else:
                    fixed = False

            return (used_2el, fixed)

        #Find a row of length 2
        missing_bit_positions = []
        row_index = None
        for i in range(len(rows)):
            missing_bit_positions = [j for j in rows[i] if received_word[j] == '?' and j not in removed_column_set]

            if len(missing_bit_positions) == 2:
                row_index = i
                break

        #no more rows of weight 2. Decoding failed.
        if len(missing_bit_positions) != 2:
            #Substitute back the removed variables
            for i in substitution_dict:
                index, constant = substitution_dict[i]
                if received_word[index] != '?':
                    received_word[i] = received_word[index] ^ constant
            return (used_2el, False)

        #remove row of weight 2
        removed_row_set.add(row_index)
        used_2el = True
        column_index = missing_bit_positions[0]
        removed_column_set.add(column_index)

        constant_list = [received_word[j] for j in rows[row_index] if received_word[j] != '?']
        constant = check_node_value[row_index]
        if len(constant_list) != 0:
            constant ^=  reduce(lambda x,y: x^y, constant_list)

        for j in cols[column_index]:
            check_node_value[j] ^= constant
            if j in cols[missing_bit_positions[1]]:
                rows[j].remove(missing_bit_positions[1])
                cols[missing_bit_positions[1]].remove(j)
            else:
                cols[missing_bit_positions[1]].append(j)
                rows[j].append(missing_bit_positions[1])



        substitution_dict[missing_bit_positions[0]] = (missing_bit_positions[1], constant)



def get_index(list, elem):
    for i in range(len(list)):
        if list[i] == elem:
            return i

    return None

'''
Tries to fix erasures from the received codeword by solving a linear system of equations
using Gaussian elimination.
Input: parity-check matrix of rows and columns, the received codeword.
Returns fully fixed codeword if system had a unique solution otherwise returns None.
'''
def Gaussian_decoding(parity_mat, received_codeword):
    rows = parity_mat[0]
    cols = parity_mat[1]
    #Preprocess parity-check matrix for equation solving
    coefficient_rows = [[] for i in range(len(rows))]
    coefficient_cols = []
    constants_col_vector = [0]*len(rows)

    missing_value_positions = []

    for i in range(len(received_codeword)):
        bit = received_codeword[i]

        if bit == 0:
            continue
        elif bit == 1:
            for index in cols[i]:
                constants_col_vector[index] ^= 1
        else:
            coefficient_cols.append(cols[i][:])
            for index in cols[i]:
                coefficient_rows[index].append(len(coefficient_cols)-1)

            missing_value_positions.append(i)

    #Solve linear system of equations
    solution = solve_linear_system(coefficient_rows, coefficient_cols, constants_col_vector)

    if solution == None:
        return None

    #Fix codeword
    for i in range(len(solution)):
        received_codeword[missing_value_positions[i]] = solution[i]

    return received_codeword


# k = 50    #size
# p = 0.3   #erasure probability
#
#
# parity_mat = generate_parity_matrix(6, 3, k)
#
# code_word = find_code_word(parity_mat)
# code_word = get_zero_codeword(parity_mat)
#
# print "       code word: " + str(code_word)
#
# received_word = BEC_simulator(code_word, p)
# print "   received word: " + str(received_word)
#
#
# two_elimination_decoding( parity_mat, received_word)
# print "decoded word(2E): " + str(received_word)
#
# print "decoded word(GD): " + str(Gaussian_decoding(parity_mat, received_word))

